﻿using System;
using System.Collections.Generic;
using CodeCamp.Demo.Domain.Events;
using CodeCamp.Demo.Domain.Order.Entities;
using Newtonsoft.Json;

namespace CodeCamp.Demo.Domain.Order
{
    public class Order
    {
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("country")] public string Country { get; set; }

        [JsonProperty("customerId")]  public string CustomerId { get; set; }

        [JsonProperty("addedAt")]  public DateTime AddedAt { get; set; }

        [JsonProperty("lines")] public List<OrderLine> Lines { get; set; }

        [JsonProperty("processedLines")] public List<ProcessedOrderLine> ProcessedLines { get; set; }

        [JsonProperty("status")] public Status Status { get; set; }

        public IEnumerable<IDomainEvent> ProcessStarted()
        {
            Status = Status.InProgress;

            yield return new OrderStartedToBeProcessedEvent
            {
                Id = Id,
                CustomerId = CustomerId
            };
        }

        public IEnumerable<IDomainEvent> ProcessFinished()
        {
            Status = Status.Finished;

            yield return new OrderFinishedToBeProcessedEvent()
            {
                Id = Id,
                CustomerId = CustomerId,
                ProcessedLineCount = ProcessedLines.Count
            };
        }

        public void LineProcessed(OrderLine orderLine)
        {
            if (ProcessedLines == null)
            {
                ProcessedLines = new List<ProcessedOrderLine>();
            }

            if (orderLine.ProductId == "23")
            {
                throw new InvalidOperationException($"Error when processing order line with id {orderLine.ProductId}");
            }

            var processedOrderLine = new ProcessedOrderLine
            {
                Id = orderLine.Id,
                ProcessedAt = DateTime.Now
            };

            ProcessedLines.Add(processedOrderLine);
        }
    }

    public enum Status
    {
        Draft,
        InProgress,
        Finished
    }
}