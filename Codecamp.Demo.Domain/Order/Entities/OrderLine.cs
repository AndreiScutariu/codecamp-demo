﻿using System;
using Newtonsoft.Json;

namespace CodeCamp.Demo.Domain.Order.Entities
{
    public class OrderLine
    {
        [JsonProperty("id")] public string Id => $"{this.ProviderId}_{this.ProductId}";

        [JsonProperty("providerId")] public string ProviderId { get; set; }

        [JsonProperty("productId")] public string ProductId { get; set; }
    }

    public class ProcessedOrderLine
    {
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("at")] public DateTime ProcessedAt { get; set; }
    }
}