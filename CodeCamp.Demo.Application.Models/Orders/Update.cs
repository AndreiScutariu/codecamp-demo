﻿using System.Collections.Generic;

namespace CodeCamp.Demo.Application.Models.Orders
{
    public class UpdateOrderModelRequest
    {
        public List<OrderLineModel> Lines { get; set; }

        public class OrderLineModel
        {
            public string ProviderId { get; set; }

            public string ProductId { get; set; }
        }
    }

    public class UpdateOrderModelResponse
    {
        public string Id { get; set; }
    }
}