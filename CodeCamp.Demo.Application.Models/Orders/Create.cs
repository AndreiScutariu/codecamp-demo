﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeCamp.Demo.Application.Models.Orders
{
    public class CreateOrderModelRequest
    {
        public string CustomerId { get; set; }

        public List<OrderLineModel> Lines { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public class OrderLineModel
        {
            public string ProviderId { get; set; }

            public string ProductId { get; set; }
        }
    }

    public class CreateOrderModelResponse
    {
        public string Id { get; set; }
    }
}