﻿namespace CodeCamp.Demo.Application.Models.Orders
{
    public class ProcessOrderModelRequest
    {
    }

    public class ProcessOrderModelResponse
    {
    }

    public class ProcessOrderLinesModelResponse
    {
        public string Id { get; set; }

        public int ProcessedLines { get; set; }
    }
}