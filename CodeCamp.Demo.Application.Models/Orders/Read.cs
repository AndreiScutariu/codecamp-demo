﻿namespace CodeCamp.Demo.Application.Models.Orders
{
    public class ReadOrderModelRequest
    {
        public string Id { get; set; }
    }

    public class ReadOrderModelResponse
    {
        public string Status { get; set; }

        public int LinesCount { get; set; }

        public string Country { get; set; }

        public string Name { get; set; }

        public string Id { get; set; }
    }
}