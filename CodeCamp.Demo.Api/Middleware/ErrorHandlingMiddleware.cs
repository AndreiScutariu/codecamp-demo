﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CodeCamp.Demo.Api.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                const HttpStatusCode code = HttpStatusCode.InternalServerError;
                var message = ex.Message;

                var result = JsonConvert.SerializeObject(new {errors = new List<string> {message}});

                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int) code;
                await context.Response.WriteAsync(result);
            }
        }
    }
}