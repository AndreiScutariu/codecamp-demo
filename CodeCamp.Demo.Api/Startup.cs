﻿using System;
using System.IO;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using CodeCamp.Demo.Api.Middleware;
using CodeCamp.Demo.DependencyContainer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace CodeCamp.Demo.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(
                c =>
                {
                    c.SwaggerDoc(
                        "v1",
                        new Info { Title = "Orders API", Version = "v1", Description = "Code Camp Demo" });

                    var xmlFile = $"{Assembly.GetEntryAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                    c.IncludeXmlComments(xmlPath);
                    c.CustomSchemaIds(type => type.FullName);
                    c.EnableAnnotations();
                });

            services.AddLogging();

            var builder = new ContainerBuilder();
            builder.Populate(services);

            builder.RegisterModule(new ContainerModule(new Configuration
            {
                ServiceBusConnectionString = this.Configuration["ServiceBusConnectionString"],
                DatabaseEndpoint = this.Configuration["DatabaseEndpoint"],
                DatabaseKey = this.Configuration["DatabaseKey"],
                DatabaseName = this.Configuration["DatabaseName"],
                SearchDatabaseCString = this.Configuration["SearchDatabaseCString"],
                SearchServiceApiKey = this.Configuration["SearchServiceApiKey"],
                SearchServiceName = this.Configuration["SearchServiceName"],
            }));

            return new AutofacServiceProvider(builder.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("../swagger/v1/swagger.json", "CodeCamp Orders API"); });

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            app.UseMvc();
        }
    }
}