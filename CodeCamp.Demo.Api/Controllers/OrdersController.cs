﻿using System.Net;
using System.Threading.Tasks;
using CodeCamp.Demo.Application.Models.Orders;
using CodeCamp.Demo.Application.Services.Common;
using CodeCamp.Demo.Application.Services.Orders.Features;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace CodeCamp.Demo.Api.Controllers
{
    [Route("api/orders")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrdersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        ///     Search orders
        /// </summary>
        /// <response code="200">Request processed correctly</response>
        /// <returns>
        ///     Order model response
        /// </returns>
        //[SwaggerResponse((int) HttpStatusCode.OK, Type = typeof(CommandResponse<SearchOrdersModelRequest>))]
        //[HttpPost("search")]
        //public async Task<IActionResult> Search([FromBody] SearchOrdersModelRequest request)
        //{
        //    return Ok();
        //}

        /// <summary>
        ///     Get order by id
        /// </summary>
        /// <response code="200">Request processed correctly</response>
        /// <returns>
        ///     Order model response
        /// </returns>
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(CommandResponse<ReadOrderModelResponse>))]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var command = new ReadOrderCommand(new ReadOrderModelRequest { Id = id });
            var response = await _mediator.Send(command);
            return Ok(response);
        }

        /// <summary>
        ///     Create an order
        /// </summary>
        /// <response code="200">Request processed correctly</response>
        /// <returns>
        ///     Order model response
        /// </returns>
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(CommandResponse<CreateOrderModelResponse>))]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateOrderModelRequest request)
        {
            var command = new CreateOrderCommand(request);
            var response = await _mediator.Send(command);
            return Ok(response);
        }

        /// <summary>
        ///     Start to process an order
        /// </summary>
        /// <response code="200">Request processed correctly</response>
        /// <returns>
        ///     Order model response
        /// </returns>
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(CommandResponse<CreateOrderModelResponse>))]
        [HttpPost("{id}/process")]
        public async Task<IActionResult> Process(string id, [FromBody] ProcessOrderModelRequest request)
        {
            var command = new StartProcessOrderCommand(id, request);
            var response = await _mediator.Send(command);
            return Ok(response);
        }

        /// <summary>
        ///     Update an order
        /// </summary>
        /// <response code="200">Request processed correctly</response>
        /// <returns>
        ///     Order model response
        /// </returns>
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(CommandResponse<UpdateOrderModelRequest>))]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] UpdateOrderModelRequest request)
        {
            var command = new UpdateOrderCommand(id, request);
            var response = await _mediator.Send(command);
            return Ok(response);
        }
    }
}