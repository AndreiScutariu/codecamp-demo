﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace CodeCamp.Demo.Api
{
    public class Program
    {
        private static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args).UseStartup<Startup>()
                .UseApplicationInsights()
                .ConfigureLogging((hostingContext, logging) => { logging.AddConsole(); }).Build();

        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }
    }
}