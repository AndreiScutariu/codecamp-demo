﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json;

namespace CodeCamp.Demo.DataAccess.Common
{
    public static class QueryFeedOptions
    {
        public static FeedOptions Default =
            new FeedOptions {MaxItemCount = 5000, MaxBufferedItemCount = -1, MaxDegreeOfParallelism = -1};
    }

    public abstract class AzureCosmosDbRepository<T>
    {
        protected readonly string CollectionId;

        protected readonly string DatabaseId;

        protected readonly IDocumentClient DocumentClient;

        protected AzureCosmosDbRepository(IDocumentClient documentClient, string databaseId, string collectionId)
        {
            DocumentClient = documentClient;
            DatabaseId = databaseId;
            CollectionId = collectionId;
        }

        protected async Task<IEnumerable<T>> Get()
        {
            var query = DocumentClient.CreateDocumentQuery<T>(
                UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId),
                QueryFeedOptions.Default).AsDocumentQuery();

            var results = new List<T>();

            while (query.HasMoreResults) results.AddRange(await query.ExecuteNextAsync<T>());

            return results;
        }

        protected async Task<T> GetById(string id)
        {
            Document document =
                await DocumentClient.ReadDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));
            return JsonConvert.DeserializeObject<T>(document.ToString());
        }

        protected async Task<IEnumerable<T>> Get(Expression<Func<T, bool>> filter)
        {
            var query = DocumentClient.CreateDocumentQuery<T>(
                UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId),
                QueryFeedOptions.Default).Where(filter).AsDocumentQuery();

            var results = new List<T>();
            while (query.HasMoreResults) results.AddRange(await query.ExecuteNextAsync<T>());

            return !results.Any() ? new List<T>() : results;
        }

        protected async Task<Document> SaveItem(T item)
        {
            return await DocumentClient.CreateDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId),
                item);
        }

        protected async Task Save(IEnumerable<T> items)
        {
            foreach (var item in items)
                await DocumentClient.CreateDocumentAsync(
                    UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId),
                    item);
        }

        protected async Task<Document> UpdateItem(string id, T item)
        {
            var documentUri = UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id);
            return await DocumentClient.ReplaceDocumentAsync(documentUri, item);
        }

        protected async Task<Document> Remove(string id)
        {
            Document document =
                await DocumentClient.ReadDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));

            return await DocumentClient.DeleteDocumentAsync(document.SelfLink);
        }
    }
}