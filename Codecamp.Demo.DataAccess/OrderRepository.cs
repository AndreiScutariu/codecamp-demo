﻿using System.Threading.Tasks;
using CodeCamp.Demo.DataAccess.Common;
using CodeCamp.Demo.Domain.Order;
using CodeCamp.Demo.Domain.Services;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace CodeCamp.Demo.DataAccess
{
    public class OrderRepository : AzureCosmosDbRepository<Order>, IOrderRepository
    {
        public OrderRepository(IDocumentClient documentClient, string databaseId, string collectionId) : base(documentClient, databaseId, collectionId)
        {
        }

        public async Task<string> Save(Order entity)
        {
            var document = await base.SaveItem(entity);
            return document.Id;
        }

        public async Task<Order> Get(string id)
        {
            return await base.GetById(id);
        }

        public async Task<string> Update(string id, Order entity)
        {
            var document = await base.UpdateItem(id, entity);
            return document.Id;
        }
    }
}