﻿using System;
using MediatR;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;

namespace CodeCamp.Demo.FunctionApps
{
    public static class TimerTriggerFunctionHandler
    {
        [FunctionName("TimerTriggerFunctionHandler")]
        public static void Run([TimerTrigger("0 * * * * *")] TimerInfo myTimer, TraceWriter log)
        {
            log.Info($"Triggered at {DateTime.Now}");
        }
    }
}
