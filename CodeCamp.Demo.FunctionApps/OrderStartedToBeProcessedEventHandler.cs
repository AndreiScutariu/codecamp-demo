﻿using System;
using System.IO;
using System.Text;
using Autofac;
using CodeCamp.Demo.Application.Services.Orders.Features;
using CodeCamp.Demo.Domain.Events;
using MediatR;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

namespace CodeCamp.Demo.FunctionApps
{
    public static class OrderStartedToBeProcessedEventHandler
    {
        [FunctionName("OrderStartedToBeProcessedEventHandler")]
        public static void Run(
            [ServiceBusTrigger(topicName: "orderstartedtobeprocessedevent", subscriptionName: "CodeCamp.Demo.FunctionApps", Connection = "ServiceBusConnectionString")]
            BrokeredMessage message,
            TraceWriter log)
        {
            log.Info($"{message.MessageId}");

            new FunctionRunner().Run(c =>
            {
                var commandString = new StreamReader(message.GetBody<Stream>(), Encoding.UTF8).ReadToEnd();
                var ev = JsonConvert.DeserializeObject<OrderStartedToBeProcessedEvent>(commandString);

                var mediator = c.Resolve<IMediator>();
                var command = new ProcessOrderLinesCommand(ev.Id);
                var response = mediator.Send(command).GetAwaiter().GetResult();

                log.Info($"Finish to process order {response.Data}");
            });
        }
    }
}