﻿using System;
using Autofac;
using CodeCamp.Demo.DependencyContainer;

namespace CodeCamp.Demo.FunctionApps
{
    public class FunctionRunner
    {
        private ContainerBuilder _containerBuilder;

        private IContainer _container;

        public static Configuration Config = new Configuration
        {
            DatabaseEndpoint = GetEnvironmentVariable("DatabaseEndpoint"),
            DatabaseKey = GetEnvironmentVariable("DatabaseKey"),
            DatabaseName = GetEnvironmentVariable("DatabaseName"),
            ServiceBusConnectionString = GetEnvironmentVariable("ServiceBusConnectionString")
        };

        public void Run(Action<IContainer> action)
        {
            this._containerBuilder = new ContainerBuilder();
            this._containerBuilder.RegisterModule(new ContainerModule(Config));
            this._container = this._containerBuilder.Build();

            using (this._container)
            {
                action(this._container);
            }
        }

        private static string GetEnvironmentVariable(string name)
        {
            var variable = Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process);

            if (string.IsNullOrEmpty(variable))
            {
                throw new Exception($"{name} environment variable should be set in the app settings!");
            }

            return variable;
        }
    }
}