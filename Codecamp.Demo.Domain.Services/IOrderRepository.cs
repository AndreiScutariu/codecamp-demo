﻿using System.Threading.Tasks;

namespace CodeCamp.Demo.Domain.Services
{
    public interface IOrderRepository
    {
        Task<string> Save(Order.Order entity);

        Task<Order.Order> Get(string id);

        Task<string> Update(string id, Order.Order entity);
    }
}