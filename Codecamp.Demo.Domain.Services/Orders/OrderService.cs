﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeCamp.Demo.Domain.Events;

namespace CodeCamp.Demo.Domain.Services.Orders
{
    public class OrderService
    {
        private readonly IOrderRepository _repository;

        public OrderService(IOrderRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<IDomainEvent>> StartProcess(string id)
        {
            var entity = await _repository.Get(id);
            var events = entity.ProcessStarted().ToList();
            
            // other logic here, if needed 
            // do something with this events in this context

            await _repository.Update(id, entity);
            return events;
        }

        public async Task<IEnumerable<IDomainEvent>> Process(string id)
        {
            var entity = await _repository.Get(id);

            foreach (var orderLine in entity.Lines)
            {
                // complex code
                await Task.Delay(TimeSpan.FromSeconds(1));

                entity.LineProcessed(orderLine);
            }

            var events = entity.ProcessFinished().ToList();
            await _repository.Update(id, entity);
            return events;
        }
    }
}