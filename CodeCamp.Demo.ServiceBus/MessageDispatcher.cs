﻿using System.Text;
using System.Threading.Tasks;
using CodeCamp.Demo.Application.Services;
using CodeCamp.Demo.Domain.Events;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using Message = Microsoft.Azure.ServiceBus.Message;

namespace CodeCamp.Demo.ServiceBus
{
    public class MessageDispatcher : IMessageDispatcher
    {
        private readonly string _connectionString;

        public MessageDispatcher(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Publish(IDomainEvent e)
        {
            var topicClient = new TopicClient(_connectionString, e.GetType().Name);

            await topicClient.SendAsync(new Message(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(e))));
        }

        public Task Send(IDomainEvent e)
        {
            throw new System.NotImplementedException();
        }
    }
}