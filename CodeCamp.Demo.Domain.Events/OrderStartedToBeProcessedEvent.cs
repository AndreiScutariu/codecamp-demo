﻿namespace CodeCamp.Demo.Domain.Events
{
    public class OrderStartedToBeProcessedEvent : IDomainEvent
    {
        public string Id { get; set; }

        public string CustomerId { get; set; }
    }
}