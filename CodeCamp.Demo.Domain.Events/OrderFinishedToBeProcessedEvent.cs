﻿namespace CodeCamp.Demo.Domain.Events
{
    public class OrderFinishedToBeProcessedEvent : IDomainEvent
    {
        public string Id { get; set; }

        public string CustomerId { get; set; }
        public int ProcessedLineCount { get; set; }
    }
}