﻿using System;
using System.Collections.Generic;
using Autofac;
using CodeCamp.Demo.Application.Services;
using CodeCamp.Demo.Application.Services.Orders.Features;
using CodeCamp.Demo.DataAccess;
using CodeCamp.Demo.Domain.Services;
using CodeCamp.Demo.ServiceBus;
using MediatR;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CodeCamp.Demo.DependencyContainer
{
    public class ContainerModule : Module
    {
        private static readonly object Locker = new object();

        private static readonly ConnectionPolicy ConnectionPolicy = new ConnectionPolicy
        {
            ConnectionMode = ConnectionMode.Direct,
            ConnectionProtocol = Protocol.Tcp,
            RequestTimeout = new TimeSpan(1, 0, 0),
            MaxConnectionLimit = 1000,
            RetryOptions = new RetryOptions
            {
                MaxRetryAttemptsOnThrottledRequests = 10,
                MaxRetryWaitTimeInSeconds = 60
            }
        };

        private static Lazy<DocumentClient> _documentClient;

        private readonly Configuration _configuration;

        public ContainerModule(Configuration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var docClient = InitDocumentClient();

            builder.RegisterInstance(new MessageDispatcher(this._configuration.ServiceBusConnectionString)).As<IMessageDispatcher>().AsSelf().SingleInstance();

            builder.Register(c =>
                    new OrderRepository(docClient, _configuration.DatabaseName, "orders"))
                .As<IOrderRepository>();

            RegisterMediator(builder);
        }

        private static void RegisterMediator(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<Mediator>().As<IMediator>().InstancePerLifetimeScope();

            containerBuilder.Register<SingleInstanceFactory>(
                ctx =>
                {
                    var c = ctx.Resolve<IComponentContext>();
                    return t => c.TryResolve(t, out var o) ? o : null;
                }).InstancePerLifetimeScope();

            containerBuilder.Register<MultiInstanceFactory>(
                ctx =>
                {
                    var c = ctx.Resolve<IComponentContext>();
                    return t => (IEnumerable<object>) c.Resolve(typeof(IEnumerable<>).MakeGenericType(t));
                }).InstancePerLifetimeScope();

            containerBuilder.RegisterAssemblyTypes(typeof(CreateOrderCommand).Assembly).AsImplementedInterfaces();
        }

        private DocumentClient InitDocumentClient()
        {
            lock (Locker)
            {
                if (_documentClient == null)
                {
                    var documentClient = new DocumentClient(
                        new Uri(_configuration.DatabaseEndpoint),
                        _configuration.DatabaseKey,
                        new JsonSerializerSettings {ContractResolver = new CamelCasePropertyNamesContractResolver()},
                        ConnectionPolicy);

                    _documentClient = new Lazy<DocumentClient>(() => documentClient);
                }
            }

            return _documentClient.Value;
        }
    }
}