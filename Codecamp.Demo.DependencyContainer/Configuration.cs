﻿namespace CodeCamp.Demo.DependencyContainer
{
    public class Configuration
    {
        public string ServiceBusConnectionString { get; set; }

        public string DatabaseKey { get; set; }

        public string DatabaseEndpoint { get; set; }

        public string DatabaseName { get; set; }

        public string SearchServiceApiKey { get; set; }

        public string SearchServiceName { get; set; }

        public string SearchDatabaseCString { get; set; }
    }
}