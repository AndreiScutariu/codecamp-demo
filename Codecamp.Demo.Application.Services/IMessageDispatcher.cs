﻿using System.Threading.Tasks;
using CodeCamp.Demo.Domain.Events;

namespace CodeCamp.Demo.Application.Services
{
    public interface IMessageDispatcher
    {
        Task Publish(IDomainEvent e);

        Task Send(IDomainEvent e);
    }
}