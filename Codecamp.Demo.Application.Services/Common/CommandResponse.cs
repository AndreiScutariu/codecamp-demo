﻿using System;
using System.Collections.Generic;

namespace CodeCamp.Demo.Application.Services.Common
{
    public class CommandResponse<T>
    {
        private CommandResponse(bool status)
        {
            IsSuccessful = status;
        }

        private CommandResponse(bool status, T data)
        {
            IsSuccessful = status;
            Data = data;
        }

        private CommandResponse(bool status, string field, string error)
        {
            IsSuccessful = status;
            Errors = new List<Tuple<string, string>> { new Tuple<string, string>(field, error) };
        }

        public IList<Tuple<string, string>> Errors { get; set; }

        public bool IsSuccessful { get; }

        public T Data { get; }

        public static CommandResponse<T> Fail(string field, string error)
        {
            var response =
                new CommandResponse<T>(false)
                {
                    Errors = new List<Tuple<string, string>>
                    {
                        new Tuple<string, string>(field, error)
                    }
                };

            return response;
        }

        public static CommandResponse<T> Ok(T responseObject)
        {
            return new CommandResponse<T>(true, responseObject);
        }
    }
}