﻿using System.Threading;
using System.Threading.Tasks;
using CodeCamp.Demo.Application.Models.Orders;
using CodeCamp.Demo.Application.Services.Common;
using CodeCamp.Demo.Domain.Services;
using CodeCamp.Demo.Domain.Services.Orders;
using MediatR;

namespace CodeCamp.Demo.Application.Services.Orders.Features
{
    public class StartProcessOrderCommand : IRequest<CommandResponse<ProcessOrderModelResponse>>
    {
        public StartProcessOrderCommand(string id, ProcessOrderModelRequest request)
        {
            Id = id;
            Request = request;
        }

        public string Id { get; }

        public ProcessOrderModelRequest Request { get; }
    }

    public class ProcessOrderLinesCommand : IRequest<CommandResponse<ProcessOrderLinesModelResponse>>
    {
        public ProcessOrderLinesCommand(string id)
        {
            Id = id;
        }

        public string Id { get; }
    }

    internal class ProcessOrderHandler : IRequestHandler<StartProcessOrderCommand, CommandResponse<ProcessOrderModelResponse>>,
        IRequestHandler<ProcessOrderLinesCommand, CommandResponse<ProcessOrderLinesModelResponse>>
    {
        private readonly IOrderRepository _repository;
        private readonly IMessageDispatcher _messageDispatcher;

        public ProcessOrderHandler(IOrderRepository repository, IMessageDispatcher messageDispatcher)
        {
            _repository = repository;
            _messageDispatcher = messageDispatcher;
        }

        public async Task<CommandResponse<ProcessOrderModelResponse>> Handle(StartProcessOrderCommand request, CancellationToken cancellationToken)
        {
            var orderService = new OrderService(_repository);
            var events = await orderService.StartProcess(request.Id);

            foreach (var ev in events)
            {
                await _messageDispatcher.Publish(ev);
            }

            return CommandResponse<ProcessOrderModelResponse>.Ok(new ProcessOrderModelResponse());
        }

        public async Task<CommandResponse<ProcessOrderLinesModelResponse>> Handle(ProcessOrderLinesCommand request, CancellationToken cancellationToken)
        {
            var orderService = new OrderService(_repository);
            var events = await orderService.Process(request.Id);

            foreach (var ev in events)
            {
                await _messageDispatcher.Publish(ev);
            }

            var entity = await _repository.Get(request.Id);

            return CommandResponse<ProcessOrderLinesModelResponse>.Ok(new ProcessOrderLinesModelResponse
            {
                Id = request.Id,
                ProcessedLines = entity.ProcessedLines.Count
            });
        }
    }
}