﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CodeCamp.Demo.Application.Models.Orders;
using CodeCamp.Demo.Application.Services.Common;
using CodeCamp.Demo.Domain.Order;
using CodeCamp.Demo.Domain.Order.Entities;
using CodeCamp.Demo.Domain.Services;
using MediatR;

namespace CodeCamp.Demo.Application.Services.Orders.Features
{
    public class UpdateOrderCommand : IRequest<CommandResponse<UpdateOrderModelResponse>>
    {
        public UpdateOrderCommand(string id, UpdateOrderModelRequest request)
        {
            Id = id;
            Request = request;
        }

        public string Id { get; }

        public UpdateOrderModelRequest Request { get; }
    }

    internal class UpdateOrderHandler : IRequestHandler<UpdateOrderCommand, CommandResponse<UpdateOrderModelResponse>>
    {
        private readonly IOrderRepository _repository;

        public UpdateOrderHandler(IOrderRepository repository)
        {
            _repository = repository;
        }

        public async Task<CommandResponse<UpdateOrderModelResponse>> Handle(UpdateOrderCommand request,
            CancellationToken cancellationToken)
        {
            var entity = await this._repository.Get(request.Id);
            var id = await this._repository.Update(request.Id, entity.Map(request));
            return CommandResponse<UpdateOrderModelResponse>.Ok(new UpdateOrderModelResponse { Id = id });
        }
    }

    internal static class UpdateOrderCommandExtensions
    {
        public static Order Map(this Order order,  UpdateOrderCommand command)
        {
            if (order.Lines == null)
            {
                order.Lines = new List<OrderLine>();
            }

            foreach (var orderLineModel in command.Request.Lines)
            {
                order.Lines.Add(new OrderLine
                {
                    ProviderId = orderLineModel.ProviderId,
                    ProductId = orderLineModel.ProductId
                });
            }

            return order;
        }
    }
}