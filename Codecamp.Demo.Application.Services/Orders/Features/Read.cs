﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CodeCamp.Demo.Application.Models.Orders;
using CodeCamp.Demo.Application.Services.Common;
using CodeCamp.Demo.Domain.Services;
using MediatR;

namespace CodeCamp.Demo.Application.Services.Orders.Features
{
    public class ReadOrderCommand : IRequest<CommandResponse<ReadOrderModelResponse>>
    {
        public ReadOrderCommand(ReadOrderModelRequest request)
        {
            Request = request;
        }

        public ReadOrderModelRequest Request { get; }
    }

    internal class ReadOrderHandler : IRequestHandler<ReadOrderCommand, CommandResponse<ReadOrderModelResponse>>
    {
        private readonly IOrderRepository _repository;

        public ReadOrderHandler(IOrderRepository repository)
        {
            _repository = repository;
        }

        public async Task<CommandResponse<ReadOrderModelResponse>> Handle(ReadOrderCommand request,
            CancellationToken cancellationToken)
        {
            var entity = await this._repository.Get(request.Request.Id);
            return CommandResponse<ReadOrderModelResponse>.Ok(new ReadOrderModelResponse
            {
                Id = entity.Id,
                Name = entity.Name, 
                Country = entity.Country,
                LinesCount = entity.Lines?.Count ?? 0,
                Status = entity.Status.ToString()
            });
        }
    }
}