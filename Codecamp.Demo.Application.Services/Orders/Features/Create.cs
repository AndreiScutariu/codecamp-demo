﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CodeCamp.Demo.Application.Models.Orders;
using CodeCamp.Demo.Application.Services.Common;
using CodeCamp.Demo.Domain.Order;
using CodeCamp.Demo.Domain.Order.Entities;
using CodeCamp.Demo.Domain.Services;
using MediatR;

namespace CodeCamp.Demo.Application.Services.Orders.Features
{
    public class CreateOrderCommand : IRequest<CommandResponse<CreateOrderModelResponse>>
    {
        public CreateOrderCommand(CreateOrderModelRequest request)
        {
            Request = request;
        }

        public CreateOrderModelRequest Request { get; }
    }

    internal class CreateOrderHandler : IRequestHandler<CreateOrderCommand, CommandResponse<CreateOrderModelResponse>>
    {
        private readonly IOrderRepository _repository;

        public CreateOrderHandler(IOrderRepository repository)
        {
            _repository = repository;
        }

        public async Task<CommandResponse<CreateOrderModelResponse>> Handle(CreateOrderCommand request,
            CancellationToken cancellationToken)
        {
            var id = await this._repository.Save(request.Map());
            return CommandResponse<CreateOrderModelResponse>.Ok(new CreateOrderModelResponse { Id = id });
        }
    }

    internal static class CreateOrderCommandExtensions
    {
        public static Order Map(this CreateOrderCommand command)
        {
            return new Order
            {
                Id = Guid.NewGuid().ToString(),
                AddedAt = DateTime.Now,
                Country = command.Request.Country,
                Name = command.Request.Name,
                CustomerId = command.Request.CustomerId,
                Status = Status.Draft,
                Lines = command.Request.Lines?.Select(orderLineModel => new OrderLine
                {
                    ProductId = orderLineModel.ProductId,
                    ProviderId = orderLineModel.ProviderId
                }).ToList()
            };
        }
    }
}